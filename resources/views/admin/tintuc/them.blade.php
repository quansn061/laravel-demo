@extends('admin.layout.index')
@section('content')
    <!-- Page Content -->
    <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tin tức
                    <small>Thêm</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $value)
                            {{$value}}<br>
                        @endforeach
                    </div>
                @endif
                @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                @endif
                <form action="admin/tintuc/them" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Thể loại</label>
                        <select class="form-control" name="theloai" id="theloai">
                            @foreach($theloai as $value)
                                <option value="{{$value->id}}">{{$value->Ten}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Loại tin</label>
                        <select class="form-control" name="loaitin" id="loaitin">
                            @foreach($loaitin as $value)
                                <option value="{{$value->id}}">{{$value->Ten}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tiêu đề</label>
                        <input class="form-control" name="tieude" placeholder="Nhập tiêu đề" value="{{old('tieude')}}"/>
                    </div>
                    <div class="form-group">
                        <label>Tóm tắt</label>
                        <textarea name="tomtat" id="demo" class="ckeditor" rows="5">{{old('tomtat')}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Nội dung</label>
                        <textarea name="noidung" class="ckeditor" rows="7">{{old('noiddung')}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Hình ảnh</label>
                        <input type="file" name="hinhanh" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nổi bật</label>
                        <label class="radio-inline">
                            <input name="noibat" value="1" checked="" type="radio">Có
                        </label>
                        <label class="radio-inline">
                            <input name="noibat" value="0" type="radio">Không
                        </label>
                    </div>
                    <button type="submit" class="btn btn-default">Thêm</button>
                    <button type="reset" class="btn btn-default">Làm mới</button>
                    </form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
    <!-- /#page-wrapper -->
@endsection
@push('script')
    <script>
        $(document).ready(function () {
           $("#loaitin").attr('disabled','disabled');
           $("#theloai").change(function(){
                   var idTheLoai=$(this).val();
                   ajaxTheLoai(idTheLoai);
           });
           function ajaxTheLoai(idTheLoai){
                $("#loaitin").removeAttr('disabled');
                $.ajax({
                    url:"admin/ajax/loaitin/"+idTheLoai,
                    method:"Get",
                    async:false,
                    dataType:"html",
                    success:function (data) {
                        $("#loaitin").html(data);
                    }
                });
            }
        });
    </script>
@endpush