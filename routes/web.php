<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\TheLoai;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin','middleware' => 'auth.check'], function () {
    //theloai
    Route::group(['prefix' => 'theloai'], function () {
        //danh sach
        Route::get('danhsach', 'TheLoaiController@getDanhSach')->name('the-loai');
        //sua
        Route::match(['get', 'post'],'sua/{id}', 'TheLoaiController@sua');
        //them
        Route::match(['get', 'post'],'them', 'TheLoaiController@them');
        //xoa
        Route::get('xoa/{id}', 'TheLoaiController@getXoa');
    });
    //loaitin
    Route::group(['prefix' => 'loaitin'], function () {
        //danh sach
        Route::get('danhsach', 'LoaiTinController@getDanhSach');
        //sua
        Route::match(['get', 'post'],'sua/{id}', 'LoaiTinController@sua');
        //them
        Route::match(['get', 'post'],'them', 'LoaiTinController@them');
        //xoa
        Route::get('xoa/{id}', 'LoaiTinController@getXoa');
    });
    //tintuc
    Route::group(['prefix' => 'tintuc'], function () {
        //danh sach
        Route::get('danhsach', 'TinTucController@getDanhSach');
        //sua
        Route::get('sua/{id}', 'TinTucController@getSua');
        Route::post('sua/{id}', 'TinTucController@postSua');
        //them
        Route::get('them', 'TinTucController@getThem');
        Route::post('them', 'TinTucController@postThem');
        //xoa
        Route::get('xoa/{id}', 'TinTucController@getXoa');
    });
    Route::group(['prefix' => 'ajax'], function () {
        Route::get('loaitin/{idTheLoai}', 'AjaxController@getLoaiTin');
    });
});
Auth::routes();

Route::match(['get', 'post'],'/login', 'AuthController@login')->name('login');
