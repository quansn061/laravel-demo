<?php
namespace App\Http\Controllers;

use App\Model\TheLoai;
use Illuminate\Http\Request;
use App\Model\LoaiTin;

class LoaiTinController extends Controller
{
    public function getDanhSach()
    {

        $loaitin = LoaiTin::with(['theLoai'])->get();
        return view("admin.loaitin.danhsach", compact('loaitin'));
    }

    public function them(Request $request)
    {
        if($request->has('_token')){
            $this->validate($request,
                [
                    'ten' => 'required|unique:LoaiTin,Ten|min:3|max:100',
                    'theloai' => 'required'
                ],
                [
                    'ten.required' => 'Bạn chưa nhập tên loại tin',
                    'ten.min' => 'Tên loại tin có độ dài từ 3 cho đến 100 ký tự',
                    'ten.max' => 'Tên loại tin có độ dài từ 3 cho đến 100 ký tự',
                    'ten.unique' => 'Tên loại tin đã tồn tại',
                    'theloai.required' => 'Bạn chưa chọn thể loại'
                ]);
            $loaitin = new LoaiTin();
            $loaitin->Ten = $request->ten;
            $loaitin->TenKhongDau = changeTitle($request->ten);
            $loaitin->idTheLoai = $request->theloai;
            $loaitin->save();
            return redirect('admin/loaitin/them')->with('thongbao', 'Thêm mới thành công');
        }else{
            $theloai = TheLoai::all();
            return view("admin.loaitin.them", ['theloai' => $theloai]);
        }

    }

    public function sua(Request $request, $id)
    {
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::find($id);
        if($request->has('_token')){
            $this->validate($request, [
                'ten' => 'required|unique:loaitin,Ten|min:3|max:100'
            ],
                [
                    'ten.required' => 'Bạn chưa nhập tên thể loại',
                    'ten.unique' => 'Tên thể loại đã tồn tại',
                    'ten.min' => 'Tên thể loại có độ dài từ 3 cho đến 100 ký tự',
                    'ten.max' => 'Tên thể loại có độ dài từ 3 cho đến 100 ký tự'
                ]);
            $loaitin->Ten = $request->ten;
            $loaitin->TenKhongDau = changeTitle($request->ten);
            $loaitin->idTheLoai = $request->theloai;
            $loaitin->save();
            return redirect('admin/loaitin/sua/' . $id)->with('thongbao', 'Sửa thành công');
        }else{
            return view("admin.loaitin.sua", ['loaitin' => $loaitin, 'theloai' => $theloai]);
        }

    }

    public function getXoa($id)
    {
        $loaitin = LoaiTin::find($id);
        $loaitin->delete();
        // Force deleting a single model instance...
        //$loaitin->forceDelete();
        return redirect('admin/loaitin/danhsach')->with('thongbao', 'Bạn đã xóa thành công');
    }
}
