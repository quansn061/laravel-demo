<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TheLoai;
use App\Model\TinTuc;
use App\Model\LoaiTin;

class TinTucController extends Controller
{
    public function getDanhSach()
    {
        $tintuc = TinTuc::orderBy('id', 'DESC')->get();
        return view('admin.tintuc.danhsach', array('tintuc' => $tintuc));
    }

    public function getThem()
    {
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        return view('admin.tintuc.them', ['theloai' => $theloai, 'loaitin' => $loaitin]);
    }

    public function postThem(Request $request)
    {
        $this->validate($request, [
            'loaitin' => 'required',
            'tieude' => 'required|min:3|max:100|unique:TinTuc,TieuDe',
            'tomtat' => 'required',
            'noidung' => 'required'
        ], [
            'loaitin.required' => 'Bạn chưa chọn loại tin',
            'tieude.required' => 'Bạn chưa nhập tiêu đề',
            'tieude.min' => 'Tiêu đề phải có ít nhất 3 ký tự',
            'tieude.max' => 'Tiêu đề không lớn hơn 100 ký tự',
            'tieude.unique' => 'Tiêu đề đã tồn tại',
            'tomtat.required' => 'Bạn chưa nhập tóm tắt',
            'noidung.required' => 'Bạn chưa nhập nội dung'
        ]);
        $tintuc = new TinTuc();
        $tintuc->TieuDe = $request->tieude;
        $tintuc->TieuDeKhongDau = changeTitle($request->tieude);
        $tintuc->idLoaiTin = $request->loaitin;
        $tintuc->TomTat = $request->tomtat;
        $tintuc->NoiDung = $request->noidung;
        $tintuc->SoLuotXem = 0;
        $tintuc->NoiBat = $request->noibat;
        if ($request->hasFile('hinhanh')) {
            $file = $request->file('hinhanh');
            $extension = $file->getClientOriginalExtension();
            if ($extension != 'jpg' && $extension != 'png' && $extension != 'jpeg') {
                return redirect('admin/tintuc/them')->with('loi', 'Phần mỏ rộng của file không phù hợp');
            }
            $name = $file->getClientOriginalName();
            $tenHinh = str_random(6) . "_" . $name;
            while (file_exists("upload/tintuc/" . $tenHinh)) {
                $tenHinh = str_random(6) . "_" . $name;
            }
            $file->move('upload/tintuc/', $tenHinh);
            $tintuc->Hinh = $tenHinh;
        } else {
            $tintuc->Hinh = "";
        }
        $tintuc->save();
        return redirect('admin/tintuc/them')->with('thongbao', 'Thêm tin tức thành công')->withInput();
    }

    public function getSua($id)
    {
        $theloai = TheLoai::all();
        $loaitin = LoaiTin::all();
        $tintuc = TinTuc::find($id);
        return view('admin.tintuc.sua', ['tintuc' => $tintuc, 'theloai' => $theloai, 'loaitin' => $loaitin]);
    }

    public function postSua(Request $request, $id)
    {
        $tintuc = TinTuc::find($id);
        $this->validate($request, [
            'loaitin' => 'required',
            'tieude' => 'required|min:3|max:100',
            'tomtat' => 'required',
            'noidung' => 'required'
        ], [
            'loaitin.required' => 'Bạn chưa chọn loại tin',
            'tieude.required' => 'Bạn chưa nhập tiêu đề',
            'tieude.min' => 'Tiêu đề phải có ít nhất 3 ký tự',
            'tieude.max' => 'Tiêu đề không lớn hơn 100 ký tự',
            'tomtat.required' => 'Bạn chưa nhập tóm tắt',
            'noidung.required' => 'Bạn chưa nhập nội dung'
        ]);
        $tintuc->TieuDe = $request->tieude;
        $tintuc->TieuDeKhongDau = changeTitle($request->tieude);
        $tintuc->idLoaiTin = $request->loaitin;
        $tintuc->TomTat = $request->tomtat;
        $tintuc->NoiDung = $request->noidung;
        $tintuc->NoiBat = $request->noibat;
        if ($request->hasFile('hinhanh')) {
            $file = $request->file('hinhanh');
            $extension = $file->getClientOriginalExtension();
            if ($extension != 'jpg' && $extension != 'png' && $extension != 'jpeg') {
                return redirect('admin/tintuc/them')->with('loi', 'Phần mỏ rộng của file không phù hợp');
            }
            $name = $file->getClientOriginalName();
            $tenHinh = str_random(6) . "_" . $name;
            while (file_exists("upload/tintuc/" . $tenHinh)) {
                $tenHinh = str_random(6) . "_" . $name;
            }
            unlink("upload/tintuc" . $tintuc->Hinh);
            $file->move('upload/tintuc/', $tenHinh);
            $tintuc->Hinh = $tenHinh;
        }
        $tintuc->save();
        return redirect('admin/tintuc/sua/' . $id)->with('thongbao', 'Sửa thành công');
    }

    public function getXoa($id)
    {
        $tintuc = TinTuc::find($id);
        $tintuc->delete();
        return redirect('admin/tintuc/danhsach')->with('thongbao', 'Xóa thành công');
    }
}
