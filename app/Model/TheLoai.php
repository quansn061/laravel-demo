<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TheLoai extends Model
{
    //
    protected $table = "theloai";

    public function loaiTin()
    {
        //id la khoa chinh bang theloai
        return $this->hasMany('App\Model\LoaiTin', 'idTheLoai', 'id');
    }

    public function tinTuc()
    {
        return $this->hasManyThrough('App\TinTuc', 'App\Model\LoaiTin', 'idTheLoai', 'idLoaiTin', 'id');
    }
}
